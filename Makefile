BIN=build/cdecrypt
OBJ=build/aes.o build/cdecrypt.o build/sha1.o build/util.o
DEP=build/aes.d build/cdecrypt.d build/sha1.d build/util.d

# -Wno-sequence-point because *dst++ = dst[-d]; is only ambiguous for people who don't know how CPUs work.
CFLAGS=-std=c99 -pipe -fvisibility=hidden -Wall -Wextra -D_GNU_SOURCE -O2 -I include
# CFLAGS=-std=c99 -pipe -fvisibility=hidden -Wall -Wextra -Werror -UNDEBUG -DAES_ROM_TABLES -D_GNU_SOURCE -O2 -I include

.PHONY: all clean

all: ${BIN}

clean:
	rm -rf ${BIN} ${OBJ} ${DEP}


${BIN}: build/aes.o build/cdecrypt.o build/sha1.o build/util.o
	gcc build/aes.o build/cdecrypt.o build/sha1.o build/util.o -o build/cdecrypt

build/aes.o: ./src/aes.c
	gcc ${CFLAGS} -MMD -c src/aes.c -o build/aes.o

build/cdecrypt.o: ./src/cdecrypt.c
	gcc ${CFLAGS} -MMD -c src/cdecrypt.c -o build/cdecrypt.o

build/sha1.o: ./src/sha1.c
	gcc ${CFLAGS} -MMD -c src/sha1.c -o build/sha1.o

build/util.o: ./src/util.c
	gcc ${CFLAGS} -MMD -c src/util.c -o build/util.o
